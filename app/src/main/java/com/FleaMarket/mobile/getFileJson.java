package com.FleaMarket.mobile;


import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class getFileJson
{

    /**
     * school : 武汉大学
     * longitude : 114.368758
     * latitude : 30.55004
     * address : 湖北省武汉市武昌区珞珈山路16号
     * zan : 123
     * distance : 527米
     */

    private String school;
    private String longitude;
    private String latitude;
    private String address;
    private String zan;
    private String distance;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZan() {
        return zan;
    }

    public void setZan(String zan) {
        this.zan = zan;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }




    public static JSONArray getJson(Context context, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        JSONArray array = null;
        String str = null;
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {

            str = stringBuilder.toString();
            array = new JSONArray(str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }

    //设置jsonDate
    public static List<Map<String, String>> setData(String str) {
        try {
            List<Map<String, String>> data = new ArrayList<Map<String, String>>();
            JSONArray array = new JSONArray(str);
            int len = array.length();
            Map<String, String> map;
            for (int i = 0; i < len; i++) {
                JSONObject object = array.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("school", object.getString("school"));
                map.put("distance", object.getString("distance"));
                map.put("zan", object.getString("zan"));
                map.put("longitude", object.getString("longitude"));
                map.put("latitude", object.getString("latitude"));
                map.put("address", object.getString("address"));
                data.add(map);
            }
            return data;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }
    //json转list列表
    public static List<Map<String, String>> setListData(String str) {
        List<Map<String, String>> data = new ArrayList<Map<String, String>>();
        try {
            JSONArray array = new JSONArray(str);
            int len = array.length();
            Map<String, String> map;
            for (int i = 0; i < len; i++) {
                JSONObject object = array.getJSONObject(i);
                map = new HashMap<String, String>();
                map.put("imageId", object.getString("imageId"));
                map.put("title", object.getString("title"));
                map.put("subTitle", object.getString("subTitle"));
                map.put("type", object.getString("type"));
                data.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;

    }

}
