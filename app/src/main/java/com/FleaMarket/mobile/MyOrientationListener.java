package com.FleaMarket.mobile;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class MyOrientationListener implements SensorEventListener
{
    private SensorManager mSensorManager;
    private Context mContext;
    private Sensor mSensor;

    private float coorX;

    public MyOrientationListener(Context context)
    {
        this.mContext = context;
    }

    @SuppressWarnings("deprecation")
    public void Start()
    {
        mSensorManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager != null)
        {
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        }
        if (mSensor != null)
        {
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }


    public void Stop()
    {
        mSensorManager.unregisterListener(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onSensorChanged(SensorEvent event)
    {
        if (event.sensor.getType() == Sensor.TYPE_ORIENTATION)
        {
            float x = event.values[SensorManager.DATA_X];

            if (Math.abs(x - coorX) > 1.0 )
            {
                if (mOritentationListener != null)
                {
                    mOritentationListener.onOrienttationChanged(x);
                }
            }

            coorX = x;
        }

    }

    private onOritentationListener mOritentationListener;

    public void SetOnOritationListener(
            onOritentationListener mOritentationListener)
    {
        this.mOritentationListener = mOritentationListener;
    }

    public interface onOritentationListener
    {
        void onOrienttationChanged(float x);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }
}
