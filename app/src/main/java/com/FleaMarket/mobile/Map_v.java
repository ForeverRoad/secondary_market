package com.FleaMarket.mobile;

        import android.app.Activity;
        import java.util.ArrayList;
        import java.util.List;
        import com.FleaMarket.mobile.overlayutil.OverlayManager;
        import com.baidu.location.BDLocation;
        import com.baidu.location.BDLocationListener;
        import com.baidu.location.LocationClient;
        import com.baidu.location.LocationClientOption;
        import com.baidu.mapapi.SDKInitializer;
        import com.baidu.mapapi.map.BaiduMap;
        import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
        import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
        import com.baidu.mapapi.map.BitmapDescriptor;
        import com.baidu.mapapi.map.BitmapDescriptorFactory;
        import com.baidu.mapapi.map.InfoWindow;
        import com.baidu.mapapi.map.MapPoi;
        import com.baidu.mapapi.map.MapStatusUpdate;
        import com.baidu.mapapi.map.MapStatusUpdateFactory;
        import com.baidu.mapapi.map.MapView;
        import com.baidu.mapapi.map.Marker;
        import com.baidu.mapapi.map.MarkerOptions;
        import com.baidu.mapapi.map.MyLocationConfiguration;
        import com.baidu.mapapi.map.MyLocationData;
        import com.baidu.mapapi.map.OverlayOptions;
        import com.baidu.mapapi.map.Polyline;
        import com.baidu.mapapi.model.LatLng;
        import com.baidu.mapapi.search.core.CityInfo;
        import com.baidu.mapapi.search.core.PoiInfo;
        import com.baidu.mapapi.search.core.SearchResult;
        import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
        import com.baidu.mapapi.search.poi.PoiCitySearchOption;
        import com.baidu.mapapi.search.poi.PoiDetailResult;
        import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
        import com.baidu.mapapi.search.poi.PoiIndoorResult;
        import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
        import com.baidu.mapapi.search.poi.PoiResult;
        import com.baidu.mapapi.search.poi.PoiSearch;
        import android.content.Context;
        import android.graphics.Bitmap;
        import android.graphics.Canvas;
        import android.graphics.Color;
        import android.graphics.Paint;
        import android.graphics.Point;
        import android.graphics.drawable.BitmapDrawable;
        import android.os.Bundle;
        import android.view.Gravity;
        import android.view.View;
        import android.view.Window;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import android.widget.Toast;
        import org.json.JSONArray;

public class Map_v extends Activity  {

    private MapView mMapView;
    private BaiduMap mTestMap;
    private Object meun;
    private boolean isFirstIn = true;
    private Context context;
    //纬度
    private double latitude;
    //经度
    private double longitude;
    //定位对象
    private LocationClient mLocationClient;
    private MyLocationListener mLocationListener;
    //地图描述
    private BitmapDescriptor mlocationDescriptor;
    //方位监听器
    private MyOrientationListener mOrientationListener;
    //目前位置x坐标
    private float mCurrentX;
    //标记
    private BitmapDescriptor Marker;
    //标记覆盖物
    private RelativeLayout mMarkerLay;
    //创建poi检索实例
    private PoiSearch mPoisearch = null;

    //创建poi检索监听
    public  OnGetPoiSearchResultListener poiListener = null;
    //json数据库
    private final String filename = "address.json";

    //推荐功能
    private JSONArray jsonData = null;

    //搜索Poi参数
    /** 搜索关键词 */
    private  String keyword = null;
    /** 每页容量50 */
    private int pageCapacity = 5;
    /** 第一页 */
    private int pageNum = 0;
    /** 搜索半径10km */
    private int radius = 20000;

    //json文件数据展示
    private DisplayInfo Infos = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_page_3);

        initView();//地图初始化

        this.context = this;
        initLocation();//位置初始化
        initMarker();  //标记初始化
        mTestMap.setOnMarkerClickListener(new OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker marker) {

                Bundle extraInfo = marker.getExtraInfo();
                DisplayInfo info = (DisplayInfo) extraInfo.getSerializable("info");

                ImageView iView =  (ImageView) mMarkerLay
                        .findViewById(R.id.map_info_pic);
                TextView distance =  (TextView) mMarkerLay
                        .findViewById(R.id.map_info_distance);
                TextView name =  (TextView) mMarkerLay
                        .findViewById(R.id.map_info_name);
                TextView zan =  (TextView) mMarkerLay
                        .findViewById(R.id.map_info_zan);

                //拼接图片字符串
                iView.setImageResource(info.getImageID());
                //iView.setImageResource(R.drawable.heu);

                distance.setText(info.getDistance());
                name.setText(info.getSchool());
                zan.setText(info.getZan()+"");

                TextView tView = new TextView(context);
                tView.setBackgroundResource(R.drawable.info_icon2);
                tView.setPadding(30, 25, 30, 50);
                tView.setGravity(Gravity.CENTER);

                tView.setText(info.getSchool());

                final LatLng latLng = marker.getPosition();
                Point point = mTestMap.getProjection().toScreenLocation(latLng);
                point.y -=47;					//图片在定位上方显示
                LatLng ll = mTestMap.getProjection().fromScreenLocation(point);

                InfoWindow infoWindow = new InfoWindow(tView, ll, 0);


                Button backAdr = (Button) findViewById(R.id.button3);
                int bottom = backAdr.getBottom();
                bottom+=220;
                backAdr.setBottom(bottom);


                mTestMap.showInfoWindow(infoWindow);

                mMarkerLay.setVisibility(View.VISIBLE);

                Toast.makeText(getApplicationContext(),
                        info.getSchool()+"\n"+info.getAddress()
                        ,Toast.LENGTH_SHORT)
                        .show();

                return true;
            }
        });
        mTestMap.setOnMapClickListener(new OnMapClickListener()
        {

            @Override
            public boolean onMapPoiClick(MapPoi arg0)
            {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void onMapClick(LatLng arg0)
            {

                mMarkerLay.setVisibility(View.GONE);
                mTestMap.hideInfoWindow();

            }
        });
    }

    public void onClick_push(View view)
    {
        // LoadJsonData();
        addOverlays(Infos.Infos);


    }
    private void addOverlays(List<DisplayInfo>Infos)
    {
        mTestMap.clear();
        LatLng mLatLng = null;
        Marker mMarker = null;
        OverlayOptions options;
//        mTestMap.getProjection().fromScreenLocation(new  );

        for (DisplayInfo info:Infos)
        {
            //获取经纬度
            mLatLng = new LatLng(info.getLatitude(), info.getLongitude());
            //地图操作
            options = new MarkerOptions().position(mLatLng)
                    .icon(Marker).zIndex(5);

            mMarker = (Marker) mTestMap.addOverlay(options);

            //bundle
            Bundle mBundle = new Bundle();
            mBundle.putSerializable("info", info);
            mMarker.setExtraInfo(mBundle);
        }

        MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(mLatLng);
        mTestMap.setMapStatus(msu);
    }

    private void initMarker()
    {
        Marker = BitmapDescriptorFactory.fromResource(R.drawable.point_icon);
        mMarkerLay = (RelativeLayout) findViewById(R.id.map_info_layout);
    }

    private void initLocation()
    {
        //检索实例初始化

        mLocationClient = new LocationClient(this);
        mLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mLocationListener);

        mPoisearch = PoiSearch.newInstance();
        mPoisearch.setOnGetPoiSearchResultListener(new PoiSearchResultListener());

        LocationClientOption option = new LocationClientOption();
        option.setCoorType("bd09ll");
        option.setIsNeedAddress(true);
        option.setOpenGps(true);
        option.setScanSpan(1000);
        mLocationClient.setLocOption(option);
        //位置描述
        mlocationDescriptor = BitmapDescriptorFactory
                .fromResource(R.drawable.nav_icon);

        mOrientationListener = new MyOrientationListener(context);

        mOrientationListener.SetOnOritationListener(new MyOrientationListener.onOritentationListener()
        {
            @Override
            public void onOrienttationChanged(float x)
            {
                mCurrentX = x;

            }
        });

    }

    private void initView() {
        Infos = new DisplayInfo(getApplicationContext(),filename);

        mMapView = (MapView)findViewById(R.id.bmapView);
        mTestMap = mMapView.getMap();
        MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(15.0f);
        mTestMap.setMapStatus(msu);
    }

    @Override
    protected void onDestroy()
    {
        // 销毁搜索模块
        mPoisearch.destroy();
        // 关闭定位图层
        mTestMap.setMyLocationEnabled(false);
        mMapView.onDestroy();
        mMapView = null;
        super.onDestroy();

    }
    @Override
    protected void onStart()
    {
        super.onStart();
        mTestMap.setMyLocationEnabled(true);
        if (!mLocationClient.isStarted())
        {
            mLocationClient.start();
        }
        mOrientationListener.Start();

    }
    @Override
    protected void onStop()
    {

        super.onStop();
        mTestMap.setMyLocationEnabled(true);
        mLocationClient.stop();

        mOrientationListener.Stop();

    }
    @Override
    protected void onResume()
    {
        super.onResume();

        mMapView.onResume();
    }
    @Override
    protected void onPause()
    {
        super.onPause();

        mMapView.onPause();
    }


    public void onClick_normal(View view)
    {
        mTestMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
    }

    public void onClick_timely(View view)
    {
        if (mTestMap.isTrafficEnabled())
        {
            mTestMap.setTrafficEnabled(false);
        }
        else
        {
            mTestMap.setTrafficEnabled(true);
        }
    }

    public void onClick_sate(View view)
    {
        mTestMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
    }

    public void onClick_back(View view)
    {

        LatLng latLng = new LatLng(latitude,longitude);

        MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);

        mTestMap.animateMapStatus(msu);
    }


    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu)
//    {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return true;
//
//    }
    //    @Override
//    public boolean onOptionsItemSelected(MenuItem item)
//    {
//        switch (item.getItemId()) {
//            case R.id.id_TestMap_common:
//                mTestMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
//                break;
//            case R.id.id_TestMap_sitelite:
//                mTestMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
//                break;
//            case R.id.id_TestMap_traffic:
//                if (mTestMap.isTrafficEnabled())
//                {
//                    mTestMap.setTrafficEnabled(false);
//                    item.setTitle("实时交通关闭");
//                }
//                else
//                {
//                    mTestMap.setTrafficEnabled(true);
//                    item.setTitle("实时交通开启");
//                }
//                break;
//            default:
//                break;
//        }
//        return false;
//    }
    //地点监听
    private class MyLocationListener implements BDLocationListener
    {
        @Override
        public void onReceiveLocation(BDLocation location)
        {
            MyLocationData data = new MyLocationData.Builder()
                    .direction(mCurrentX)
                    .accuracy(location.getRadius())
                    .latitude(location.getLatitude())
                    .longitude(location.getLongitude())
                    .build();


            latitude = location.getLatitude();
            longitude = location.getLongitude();

            mTestMap.setMyLocationData(data);

            MyLocationConfiguration config = new
                    MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, true,mlocationDescriptor);
            mTestMap.setMyLocationConfigeration(config);

            if (isFirstIn)
            {

                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                MapStatusUpdate msu = MapStatusUpdateFactory.newLatLng(latLng);

                mTestMap.animateMapStatus(msu);
                isFirstIn = false;

                Toast.makeText(context, location.getAddrStr(), Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void  onClick_Search(View view)
    {
        //获取输入值
        EditText searchEdit = (EditText) findViewById(R.id.editText1);
        keyword = searchEdit.getText().toString();
        mPoisearch.searchNearby(new PoiNearbySearchOption()
                .keyword(keyword)
                    .location(new LatLng(longitude, latitude))
                        .pageCapacity(pageCapacity)
                            .pageNum(pageNum)
                                .radius(radius));

        mPoisearch.searchInCity(new PoiCitySearchOption()
                .city("武汉")
                    .pageNum(0)
                        .keyword(keyword)
                            .pageCapacity(10));
    }

    public class PoiSearchResultListener implements OnGetPoiSearchResultListener
    {

        @Override
        public void onGetPoiResult(PoiResult poiResult) {
            if (poiResult == null ||poiResult.error == SearchResult.ERRORNO.RESULT_NOT_FOUND)
            {
                Toast.makeText(getApplicationContext(), "未找到结果!", Toast.LENGTH_LONG)
                        .show();
                return;
            }

            if (poiResult.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {

                // 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
                String strInfo = "在";
                for (CityInfo cityInfo : poiResult.getSuggestCityList()) {
                    strInfo += cityInfo.city;
                    strInfo += ",";
                }
                strInfo += "找到结果";
                Toast.makeText(getApplicationContext(), strInfo, Toast.LENGTH_LONG)
                        .show();
            }
            if (poiResult.error == SearchResult.ERRORNO.NO_ERROR)
            {
                //清理覆盖物
                mTestMap.clear();
                MyPoiOverLay mPoiOverlay = new MyPoiOverLay(mTestMap);
                mTestMap.setOnMarkerClickListener(mPoiOverlay);
                mPoiOverlay.setPoiResult(poiResult);
                mPoiOverlay.addToMap();
                mPoiOverlay.zoomToSpan();

                //地点列表
                List<OverlayOptions> options = mPoiOverlay.getOverlayOptions();
                mTestMap.addOverlays(options);
            }
        }

        @Override
        public void onGetPoiDetailResult(PoiDetailResult poiDetailResult) {


        }
        @Override
        public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {

        }
    }
    public class MyPoiOverLay extends OverlayManager
    {
        private PoiResult poiResult = null;

        public PoiResult getPoiResult() {
            return poiResult;
        }

        public void setPoiResult(PoiResult poiResult) {
            this.poiResult = poiResult;
        }

        public MyPoiOverLay(BaiduMap baiduMap) {
            super(baiduMap);
        }


        public MyPoiOverLay(BaiduMap baiduMap, PoiResult poiResult) {
            super(baiduMap);
            this.poiResult = poiResult;
        }
        @Override
        public List<OverlayOptions> getOverlayOptions() {

            if ((this.poiResult == null) ||(this.poiResult.getAllPoi()== null))
                return null;

            ArrayList<OverlayOptions> Arrlist = new ArrayList<OverlayOptions>();
            for (int i = 1; i < this.poiResult.getAllPoi().size();i++)
            {
                if (this.poiResult.getAllPoi().get(i).location == null)
                    continue;
                Bundle bundle = new Bundle();
                bundle.putInt("index",1);
                Arrlist.add(new MarkerOptions()
                        .icon(BitmapDescriptorFactory
                                .fromBitmap(setNumToIcon(i)))
                        .extraInfo(bundle).position(this
                                .poiResult.
                                        getAllPoi()
                                .get(i).location));
            }
            return Arrlist;
        }

        @Override
        public boolean onMarkerClick(Marker marker) {
            if (marker.getExtraInfo() != null){
                int index = marker.getExtraInfo().getInt("index");
                PoiInfo poi = poiResult.getAllPoi().get(index);

                //详情搜索
                mPoisearch.searchPoiDetail((new PoiDetailSearchOption())
                        .poiUid(poi.uid));
                return true;
            }
            return false;
        }

        @Override
        public boolean onPolylineClick(Polyline polyline) {
            return false;
        }


        private Bitmap setNumToIcon(int num)
        {
            BitmapDrawable bd = (BitmapDrawable)getResources()
                    .getDrawable(R.drawable.point_icon);
            Bitmap bitmap = bd.getBitmap().copy(Bitmap.Config.ARGB_8888,true);
            Canvas canvas = new Canvas();

            Paint paint = new Paint();
            paint.setColor(Color.rgb(0,0,0));
            paint.setAntiAlias(true);

            int widthX,heightY = 0;
            if (num<10)
            {
                paint.setTextSize(25);
                widthX = 8;
                heightY = 6;
            }
            else
            {
                paint.setTextSize(20);
                widthX = 11;
            }
            canvas.drawText(String.valueOf(num),
                    ((bitmap.getWidth()/2)-widthX),
                    ((bitmap.getHeight()/2)+heightY),paint);
            return bitmap;
        }
    }
}

