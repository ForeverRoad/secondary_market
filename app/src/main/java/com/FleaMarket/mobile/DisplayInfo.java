package com.FleaMarket.mobile;

import android.content.Context;
import android.content.res.AssetManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisplayInfo implements Serializable
{



    private String school;
    private double longitude;
    private double latitude;
    private String address;
    private int zan;
    private String distance;
    int ImageID;
    public  List<DisplayInfo> Infos = null;



    public DisplayInfo(String distance, int zan, String address, double latitude, double longitude, String school, int ImageID) {
        super();
        this.distance = distance;
        this.zan = zan;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.school = school;
        this.ImageID = ImageID;
    }

    public DisplayInfo(Context context, String fileName) {

        Infos = new ArrayList<DisplayInfo>();

        JSONArray jsonData = getJson(context,fileName);
        JSONObject json = null;
        try {
            for (int i = 0;i<jsonData.length();i++) {
                json = jsonData.getJSONObject(i);

                Infos.add(new DisplayInfo(
                        json.getString("distance"),
                        Integer.parseInt(json.getString("zan")),
                        json.getString("address"),
                        Double.parseDouble(json.getString("latitude")),
                        Double.parseDouble(json.getString("longitude")),
                        json.getString("school"),
                        Integer.parseInt(json.getString("imageID").substring(2, json.getString("imageID").length()), 16)));

                json = null;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getZan() {
        return zan;
    }

    public void setZan(int zan) {
        this.zan = zan;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public DisplayInfo(String school) {
        this.school = school;
    }

    public int getImageID() {
        return ImageID;
    }

    public void setImageID(int imageID) {
        ImageID = imageID;
    }

    public static JSONArray getJson(Context context, String fileName) {

        StringBuilder stringBuilder = new StringBuilder();
        JSONArray array = null;
        String str = null;
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        try {
            str = stringBuilder.toString();
            array = new JSONArray(str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }
}
