package com.FleaMarket.Login;

import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.FleaMarket.Login.userBean.user_inf;
import com.FleaMarket.mobile.R;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;


public class Sign_in extends Activity {

    private String userNum;
    private String pwd;
    private String school;
    private String tel;
    private String enter_pwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);


        Button Reg1 = (Button) findViewById(R.id.Reg2);
        Reg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //数据库添加数据
                signIn();
                Intent intent = new Intent(Sign_in.this, Login.class);
                startActivity(intent);
            }
        });

    }

    public void getData() {
        EditText tel_info = (EditText) findViewById(R.id.tel);
        EditText school_info = (EditText) findViewById(R.id.school);
        EditText userNum_info = (EditText) findViewById(R.id.userNum);
        EditText pwd_info = (EditText) findViewById(R.id.pwd);
        EditText enter_pwd_info = (EditText) findViewById(R.id.enter_pwd);

        tel = tel_info.getText().toString();
        school = school_info.getText().toString();
        pwd = pwd_info.getText().toString();
        userNum = userNum_info.getText().toString();
        school = school_info.getText().toString();
        enter_pwd = enter_pwd_info.getText().toString();
    }


    public void signIn() {
        getData();
        if (tel.equals("") || school.equals("") || pwd.equals("") || userNum.equals("") || enter_pwd.equals("")) {
            Toast.makeText(getApplicationContext(), "信息不完整", Toast.LENGTH_SHORT).show();
        }
        else {
            user_inf person = new user_inf();
            person.setPwd(pwd);
            person.setSchool(school);
            person.setTel(tel);
            person.setUserNumber(userNum);

            person.save(new SaveListener<String>() {
                @Override
                public void done(String s, BmobException e) {
                    if (e == null) {
                        Toast.makeText(getApplicationContext(),"注册成功！",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Sign_in.this,Login.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"注册失败！",Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }

//        user_inf p2 = new user_inf("usersTable",);
//        p2.setName("lucky");
//        p2.setAddress("北京海淀");
//        p2.save(this, new SaveListener() {
//            @Override
//            public void onSuccess() {
//                // TODO Auto-generated method stub
//                toast("添加数据成功，返回objectId为："+p2.getObjectId());
//            }
//
//            @Override
//            public void onFailure(int code, String msg) {
//                // TODO Auto-generated method stub
//                toast("创建数据失败：" + msg);
//            }
//        });
//    }
}
