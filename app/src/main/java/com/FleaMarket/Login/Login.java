package com.FleaMarket.Login;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.FleaMarket.Login.userBean.user_inf;
import com.FleaMarket.mobile.R;

import java.util.List;

import cn.bmob.v3.Bmob;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.QueryListener;


public class Login extends Activity {
    private String login_tel;
    private String login_pwd;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
        setContentView(R.layout.login);

        Bmob.initialize(this, "60e231385b37ab9fdcc7c2ce52d63fe0");
        initView();

         //登陆跳转
        Button Landing = (Button) findViewById(R.id.login);
        Landing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit_tel = (EditText) findViewById(R.id.login_tel_text);
                EditText edit_pwd = (EditText) findViewById(R.id.login_pwd_text);
                login_tel = edit_tel.getText().toString();
                login_pwd = edit_pwd.getText().toString();

                if (login_tel.equals("")||login_pwd.equals("")) {
                    Toast.makeText(getApplicationContext(),"请输入完整登录信息！",Toast.LENGTH_SHORT).show();
                    return;
                }
                //账号验证，进入主页
                TeVer(login_tel,login_pwd);
            }
        });

        //注册页面跳转
        TextView Reg1 = (TextView)findViewById(R.id.Reg);
        Reg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Sign_in = new Intent(Login.this, Sign_in.class);
                startActivity(Sign_in);
            }
        });
   }

    public void TeVer(String userNumber,String password)
    {
        BmobQuery<user_inf> bmobQuery = new BmobQuery<user_inf>();

        bmobQuery.addWhereEqualTo("tel",userNumber);
        bmobQuery.addWhereEqualTo("pwd",password);

        bmobQuery.findObjects(new FindListener<user_inf>() {
            @Override
            public void done(List<user_inf> usinf, BmobException e) {
                if (usinf.size() == 1) {
                    Intent FirstPage = new Intent(Login.this, com.FleaMarket.homePage.HomeActivity.class);
                    startActivity(FirstPage);
                }else {
                    Toast.makeText(getApplicationContext(),"登录失败!",Toast.LENGTH_SHORT).show();
                }
            }
        });


//        BmobQuery<user_inf> query = new BmobQuery<user_inf>();
////查询playerName叫“比目”的数据
//        query.addWhereEqualTo("tel", "123");
//
//        query.findObjects(new FindListener<user_inf>() {
//            @Override
//            public void done(List<user_inf> object, BmobException e) {
//                if(e==null){
////                                    Intent Sign_in = new Intent(Login.this, Sign_in.class);
////                                    startActivity(Sign_in);
//                    Toast.makeText(getApplicationContext(),"查找success！",Toast.LENGTH_SHORT).show();
//
//                }else{
//                    Toast.makeText(getApplicationContext(),"查找failed！",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


    }

   public void initView()
   {
       TextView Reg= (TextView)findViewById(R.id.Reg);
       Reg.setText(Html.fromHtml("<u>"+"注册用户"+"</u>"));
       TextView Rpsd = (TextView)findViewById(R.id.Rpsd);
       Rpsd.setText(Html.fromHtml("<u>"+"找回密码"+"</u>"));
   }
}
