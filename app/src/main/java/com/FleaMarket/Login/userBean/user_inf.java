package com.FleaMarket.Login.userBean;

import cn.bmob.v3.BmobObject;

public class user_inf extends BmobObject{

    private String userNumber;
    private String pwd;
    private String school;
    private String tel;
    private String tableName;

    public user_inf() {
        this.tableName = "user_inf";
    }


    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
