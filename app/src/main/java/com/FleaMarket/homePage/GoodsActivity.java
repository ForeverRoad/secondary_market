package com.FleaMarket.homePage;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

import com.FleaMarket.mobile.R;

public class GoodsActivity extends AppCompatActivity {
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);//去掉标题栏
            setContentView(R.layout.activity_goods);

            ImageButton Landing = (ImageButton) findViewById(R.id.buy_now);
            Landing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder  = new AlertDialog.Builder(GoodsActivity.this);
                    builder.setTitle("联系方式" ) ;
                    builder.setMessage("17771429576" ) ;
                    builder.setPositiveButton("确定" ,  null );
                    builder.show();
                }
            });
        }
}
