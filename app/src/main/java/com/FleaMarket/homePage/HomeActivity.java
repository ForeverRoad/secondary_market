package com.FleaMarket.homePage;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.FleaMarket.Login.Login;
import com.FleaMarket.homePage.Page.Page_1Activity;
import com.FleaMarket.homePage.Page.Page_2Activity;
import com.FleaMarket.homePage.Page.Page_3Activity;
import com.FleaMarket.homePage.Page.Page_4Activity;
import com.FleaMarket.homePage.Page.Page_5Activity;
import com.FleaMarket.homePage.Page.goods;
import com.FleaMarket.mobile.R;
import com.baidu.mapapi.SDKInitializer;

import java.util.List;

import cn.bmob.v3.BmobObject;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;


public class HomeActivity extends FragmentActivity implements View.OnClickListener{

    // 初始化顶部栏显示
    private ImageView titleLeftImv;
    private TextView titleTv;
    // 定义4个Fragment对象
    private Page_1Activity fg1;
    private Page_2Activity fg2;
    private Page_3Activity fg3;
    private Page_4Activity fg4;
    private Page_5Activity fg5;
    //货物标记物
    private FrameLayout goodsFralay;


    // 定义每个选项中的相关控件
    private RelativeLayout firstLayout;
    private RelativeLayout secondLayout;
    private RelativeLayout thirdLayout;
    private RelativeLayout fourthLayout;
    private RelativeLayout fifthLayout;
    private ImageView firstImage;
    private ImageView secondImage;
    private ImageView thirdImage;
    private ImageView fourthImage;
    private ImageView fifthImage;
    private TextView firstText;
    private TextView secondText;
    private TextView thirdText;
    private TextView fourthText;
    private TextView fifthText;
    // 定义几个颜色
    private int whirt = 0xFF00AEFF;
    private int gray = 0xFF0947B9;
    private int dark = 0xffFFFFFF;
    // 定义FragmentManager对象管理器
    private FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_home);
        fragmentManager = getSupportFragmentManager();
        initView(); // 初始化界面控件
        setChioceItem(0); // 初始化页面加载时显示第一个选项



        // 将Button 1 加入到LinearLayout 中
//        Button b1 = new Button(this);
//        b1.setText("取消");
//        showLinlay. addView ( b1 );
//
//        // 将Button 2 加入到LinearLayout 中
//        Button b2 = new Button(this);
//        b2.setText("确定");
//        showLinlay. addView ( b2 );

//        getGoods();
//        showGoods(goodlist);
    }
    /**
     * 初始化页面
     */
    private void initView() {

// 初始化底部导航栏的控件
        firstImage = (ImageView) findViewById(R.id.first_image);
        secondImage = (ImageView) findViewById(R.id.second_image);
        thirdImage = (ImageView) findViewById(R.id.third_image);
        fourthImage = (ImageView) findViewById(R.id.fourth_image);
        fifthImage = (ImageView) findViewById(R.id.fifth_image);

        firstText = (TextView) findViewById(R.id.first_text);
        secondText = (TextView) findViewById(R.id.second_text);
        thirdText = (TextView) findViewById(R.id.third_text);
        fourthText = (TextView) findViewById(R.id.fourth_text);
        fifthText = (TextView) findViewById(R.id.fifth_text);

        firstLayout = (RelativeLayout) findViewById(R.id.first_layout);
        secondLayout = (RelativeLayout) findViewById(R.id.second_layout);
        thirdLayout = (RelativeLayout) findViewById(R.id.third_layout);
        fourthLayout = (RelativeLayout) findViewById(R.id.fourth_layout);
        fifthLayout = (RelativeLayout) findViewById(R.id.fifth_layout);

        firstLayout.setOnClickListener(HomeActivity.this);
        secondLayout.setOnClickListener(HomeActivity.this);
        thirdLayout.setOnClickListener(HomeActivity.this);
        fourthLayout.setOnClickListener(HomeActivity.this);
        fifthLayout.setOnClickListener(HomeActivity.this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.first_layout:
                setChioceItem(0);
                break;
            case R.id.second_layout:
                setChioceItem(1);
                break;
            case R.id.third_layout:
                Intent Sign_in = new Intent(HomeActivity.this, com.FleaMarket.mobile.Map_v.class);
                startActivity(Sign_in);

                break;
            case R.id.fourth_layout:
                setChioceItem(3);
                break;
            case R.id.fifth_layout:
                setChioceItem(4);
                break;
            default:
                break;
        }
    }
    /**
     * 设置点击选项卡的事件处理
     *
     * @param index 选项卡的标号：0, 1, 2, 3
     */
    private void setChioceItem(int index) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        clearChioce(); // 清空, 重置选项, 隐藏所有Fragment
        hideFragments(fragmentTransaction);
        switch (index) {
            case 0:
// firstImage.setImageResource(R.drawable.XXXX); 需要的话自行修改
                firstText.setTextColor(dark);
                firstLayout.setBackgroundColor(gray);
// 如果fg1为空，则创建一个并添加到界面上
                if (fg1 == null) {
                    fg1 = new Page_1Activity();
                    fragmentTransaction.add(R.id.content, fg1);
                } else {
// 如果不为空，则直接将它显示出来
                    fragmentTransaction.show(fg1);
                }
                break;

            case 1:
// secondImage.setImageResource(R.drawable.XXXX);
                secondText.setTextColor(dark);
                secondLayout.setBackgroundColor(gray);
                if (fg2 == null) {
                    fg2 = new Page_2Activity();
                    fragmentTransaction.add(R.id.content, fg2);
                } else {
                    fragmentTransaction.show(fg2);
                }
                break;

            case 2:
// thirdImage.setImageResource(R.drawable.XXXX);
                thirdText.setTextColor(dark);
                thirdLayout.setBackgroundColor(gray);
                if (fg3 == null) {
                    fg3 = new Page_3Activity();
                    fragmentTransaction.add(R.id.content, fg3);
                } else {
                    fragmentTransaction.show(fg3);
                }
                break;

            case 3:
// fourthImage.setImageResource(R.drawable.XXXX);
                fourthText.setTextColor(dark);
                fourthLayout.setBackgroundColor(gray);
                if (fg4 == null) {
                    fg4 = new Page_4Activity();
                    fragmentTransaction.add(R.id.content, fg4);
                } else {
                    fragmentTransaction.show(fg4);
                }
                break;

            case 4:
// fourthImage.setImageResource(R.drawable.XXXX);
                fifthText.setTextColor(dark);
                fifthLayout.setBackgroundColor(gray);
                if (fg5 == null) {
                    fg5 = new Page_5Activity();
                    fragmentTransaction.add(R.id.content, fg5);
                } else {
                    fragmentTransaction.show(fg5);
                }
                break;
        }


        fragmentTransaction.commit(); // 提交
    }
    /**
     * 当选中其中一个选项卡时，其他选项卡重置为默认
     */
    private void clearChioce() {
// firstImage.setImageResource(R.drawable.XXX);
        firstText.setTextColor(gray);
        firstLayout.setBackgroundColor(whirt);
// secondImage.setImageResource(R.drawable.XXX);
        secondText.setTextColor(gray);
        secondLayout.setBackgroundColor(whirt);
// thirdImage.setImageResource(R.drawable.XXX);
        thirdText.setTextColor(gray);
        thirdLayout.setBackgroundColor(whirt);
// fourthImage.setImageResource(R.drawable.XXX);
        fourthText.setTextColor(gray);
        fourthLayout.setBackgroundColor(whirt);

        fifthText.setTextColor(gray);
        fifthLayout.setBackgroundColor(whirt);
    }
    /**
     * 隐藏Fragment
     *
     * @param fragmentTransaction
     */
    private void hideFragments(FragmentTransaction fragmentTransaction) {
        if (fg1 != null) {
            fragmentTransaction.hide(fg1);
        }
        if (fg2 != null) {
            fragmentTransaction.hide(fg2);
        }
        if (fg3 != null) {
            fragmentTransaction.hide(fg3);
        }
        if (fg4 != null) {
            fragmentTransaction.hide(fg4);
        }
        if (fg5 != null) {
            fragmentTransaction.hide(fg5);
        }
    }
    public void onFocus(View v)
    {
        v.setSelected(true);
        Intent searchPage = new Intent(HomeActivity.this, com.FleaMarket.homePage.Page.Show_Identify.class);
        startActivity(searchPage);
    }

//    public void getGoods(List<goods> lst)
//    {
//        LinearLayout layout = new LinearLayout(this); // 线性布局方式
//        for (goods gs:lst)
//        layout.setOrientation(LinearLayout.HORIZONTAL); //
//        layout.setBackgroundColor(0xffffffff);
//        LinearLayout.LayoutParams LP_MM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
//        layout.setLayoutParams(LP_MM);
//
//        ImageView imageView= new ImageView(this);
//        imageView.setBackgroundResource(R.drawable.user);
//        LinearLayout.LayoutParams PARA = new LinearLayout.LayoutParams(350,350);
//        imageView.setLayoutParams(PARA);
//        layout.addView(imageView);
//
//        TextView tv = new TextView(this); // 普通聊天对话
//        tv.setText();
//        tv.setTextSize(20);
//        tv.setBackgroundColor(Color.GRAY);
//        LinearLayout.LayoutParams LP_WW = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        tv.setLayoutParams(LP_WW);
//        layout.addView(tv);
//        layout.setPadding(0,20,0,0);
//        showlinea1.addView(layout);
//    }
    public void onClick_qixing(View v)
    {
        BmobQuery<goods> myGoods = new BmobQuery<goods>();
        myGoods.addWhereEqualTo("class","骑行神装");
        myGoods.findObjects(new FindListener<goods>() {
            @Override
            public void done(List<goods> list, BmobException e) {
//                if (list.size() != 0 ) {
//                    getGoods(list);
//                }
            }
        });
    }

}