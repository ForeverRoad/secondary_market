package com.FleaMarket.homePage;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.FleaMarket.mobile.R;

import java.util.ArrayList;
import java.util.List;

public class RelActivity extends Activity {
    private Spinner spinner;
    private List<String> data_list;
    private ArrayAdapter<String> arr_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rel);


        spinner = (Spinner) findViewById(R.id.editText8);

        //数据
        data_list = new ArrayList<String>();
        data_list.add("选择类型");
        data_list.add("骑行神装");
        data_list.add("二手车辆");
        data_list.add("电脑耗材");

        data_list.add("通讯设备");
        data_list.add("个人设计");
        data_list.add("宿舍神器");
        data_list.add("电子数码");
        data_list.add("鞋包首饰");
        data_list.add("论文报告");
        data_list.add("书山有路");

        //适配器
        arr_adapter= new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data_list);
        //设置样式
        arr_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //加载适配器
        spinner.setAdapter(arr_adapter);
    }


}
