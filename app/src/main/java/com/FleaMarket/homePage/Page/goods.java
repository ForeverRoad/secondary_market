package com.FleaMarket.homePage.Page;


import cn.bmob.v3.BmobObject;

public class goods extends BmobObject{
    private String userNum;
    private String title;
    private String tel;
    private String description;
    private String identify;
    private String address;
    private String memoryaddress;

    private String tableName;

    public goods() {
        this.tableName = "goods";
    }

    public String getUserNum() {
        return userNum;
    }

    public void setUserNum(String userNum) {
        this.userNum = userNum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getIdentify() {
        return identify;
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMemoryAddress() {
        return memoryaddress;
    }

    public void setMemoryAddress(String memoryAddress) {
        this.memoryaddress = memoryAddress;
    }
}
