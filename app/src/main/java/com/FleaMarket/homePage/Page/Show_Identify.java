package com.FleaMarket.homePage.Page;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.FleaMarket.mobile.R;

import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class Show_Identify extends AppCompatActivity {

    // 物品展现
    private LinearLayout showlinea1;
    private LinearLayout showlinea2;
    private LinearLayout showlinea3;
    private FrameLayout singleFramelay;
    private TextView address_tv;
    private TextView description_tv;
    private ImageView image_poi;
    private List<goods> goodlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_identify);

        showlinea1 = (LinearLayout) findViewById(R.id.old_lin_show);

        getGoods();
    }

    public void showGoods(List<goods> Goods) {
        String address;
        for (goods gs:Goods)
        {
            LinearLayout layout = new LinearLayout(this); // 线性布局方式
            layout.setOrientation(LinearLayout.HORIZONTAL); //
            layout.setBackgroundColor(0xffffffff);
            LinearLayout.LayoutParams LP_MM = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            layout.setLayoutParams(LP_MM);
            address = gs.getMemoryAddress();
            ImageView imageView= new ImageView(this);

            int x = Integer.parseInt(address.substring(2, address.length()), 16);
          //  imageView.setBackgroundResource(Integer.parseInt(address.substring(2, address.length()), 16));
            imageView.setBackgroundResource(R.drawable.user);
            LinearLayout.LayoutParams PARA = new LinearLayout.LayoutParams(350,350);
            imageView.setLayoutParams(PARA);
            layout.addView(imageView);

            TextView tv = new TextView(this); // 普通聊天对话
            tv.setText(gs.getDescription());
            tv.setTextSize(20);
            tv.setBackgroundColor(Color.GRAY);
            LinearLayout.LayoutParams LP_WW = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            tv.setLayoutParams(LP_WW);
            layout.addView(tv);
            layout.setPadding(0,20,0,0);
            showlinea1.addView(layout);

        }
    }

    public void getGoods()
    {
        address_tv = new TextView(this);
        BmobQuery<goods> myGoods = new BmobQuery<goods>();
        myGoods.findObjects(new FindListener<goods>() {
            @Override
            public void done(List<goods> list, BmobException e) {
                if (list.size() != 0 ) {
                    showGoods(list);
                //    goodlist = list;
                }
            }
        });
    }
}
