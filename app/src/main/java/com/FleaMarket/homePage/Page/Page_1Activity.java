package com.FleaMarket.homePage.Page;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.FleaMarket.homePage.GoodsActivity;
import com.FleaMarket.homePage.HomeActivity;
import com.FleaMarket.mobile.R;


public class Page_1Activity extends Fragment{

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_page_1, container, false);

        View home = (View) view.findViewById(R.id.linear1);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),GoodsActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}